"use strict"

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
 
const list = document.createElement("ul");
const block = document.getElementById("root");

block.append(list);

books.forEach((item, index) => {
    const listItem = document.createElement("li");

    try{
        if (!item.author) {
            throw new SyntaxError(`Нет автора ${index}ой книги в массиве`);
        } 
        if (!item.name) {
            throw new SyntaxError(`Нет названия ${index}ой книги в массиве`);
        }
        if (!item.price) {
            throw new SyntaxError(`Нет цены ${index}ой книги в массиве`);
        }
        listItem.innerHTML = `author: ${item.author}, name: ${item.name}, price: ${item.price}`;
        list.append(listItem);

    } catch(err){
        console.log(err.message);
    }
});
